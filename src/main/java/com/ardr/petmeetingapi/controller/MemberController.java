package com.ardr.petmeetingapi.controller;


import com.ardr.petmeetingapi.Service.MemberService;
import com.ardr.petmeetingapi.model.Member.MemberCreatRequest;
import com.ardr.petmeetingapi.model.Member.MemberItem;
import com.ardr.petmeetingapi.model.Member.MemberResponse;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/people")
    public String setMember(@RequestBody MemberCreatRequest request) {
        memberService.setMember(request);
        return "OK";
    }

    @GetMapping("/all")
    public List<MemberItem> getMembers() {
        return memberService.getMembers();
    }


    @GetMapping("/Member/{id}")
    public MemberResponse getMember(@PathVariable long id) {
        return memberService.getMember(id);
    }

}
