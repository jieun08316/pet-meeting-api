package com.ardr.petmeetingapi.controller;


import com.ardr.petmeetingapi.Service.MemberService;
import com.ardr.petmeetingapi.Service.PetService;
import com.ardr.petmeetingapi.entity.Member;
import com.ardr.petmeetingapi.model.Pet.PetCreatRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/pet")
public class PetController {
private final PetService petService;
private final MemberService memberService;

@PostMapping("/new/member-id/{memberId}")
    public String setPet(@PathVariable long memberId, @RequestBody PetCreatRequest request){
    Member member =memberService.getData(memberId);
    petService.setPet(member, request);
    return "OK";
}


}
