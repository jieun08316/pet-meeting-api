package com.ardr.petmeetingapi.model.Member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberResponse {
    private long id;
    private String OwnerName;
    private short age;
    private String isMan;
    private String phoneNumber;
}
