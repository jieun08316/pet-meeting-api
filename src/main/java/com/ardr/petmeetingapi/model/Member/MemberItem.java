package com.ardr.petmeetingapi.model.Member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberItem {
    private long id;
    private String OwnerName;
}
