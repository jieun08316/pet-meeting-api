package com.ardr.petmeetingapi.model.Member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberCreatRequest {

    private String OwnerName;
    private short age;
    private Boolean isMan;
    private String phoneNumber;

}
