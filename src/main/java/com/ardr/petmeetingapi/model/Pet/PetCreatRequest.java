package com.ardr.petmeetingapi.model.Pet;


import com.ardr.petmeetingapi.entity.Member;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PetCreatRequest {
    private Member member;
    private String petName;
    private String petType;
}
