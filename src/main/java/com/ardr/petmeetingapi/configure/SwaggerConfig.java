package com.ardr.petmeetingapi.configure;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@OpenAPIDefinition(
        info = @Info(title = "펫모임 App",
                description = "한달에 한번 여행가는 펫모임의 회원 반려동물을 관리하는 프로그램",
                version = "v1"))
@RequiredArgsConstructor
@Configuration
public class SwaggerConfig {

    @Bean
    public GroupedOpenApi V1OpenApi() {

        String[] paths = {"/v1/**"};

        return GroupedOpenApi.builder()
                .group("PetMeeting API v1")
                .pathsToMatch(paths)
                .build();
    }
}
