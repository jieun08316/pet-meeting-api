package com.ardr.petmeetingapi.repository;

import com.ardr.petmeetingapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
}
