package com.ardr.petmeetingapi.repository;


import com.ardr.petmeetingapi.entity.Pet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetRepository extends JpaRepository<Pet, Long> {}
