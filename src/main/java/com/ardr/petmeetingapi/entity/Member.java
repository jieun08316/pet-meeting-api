package com.ardr.petmeetingapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Member {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false, length = 20)
    private String OwnerName;

    @Column(nullable = false)
    private short age;

    @Column(nullable = false)
    private Boolean isMan;


    @Column(nullable = false, unique = true, length = 13)
    private String phoneNumber;

}
