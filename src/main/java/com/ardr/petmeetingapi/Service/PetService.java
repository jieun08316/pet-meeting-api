package com.ardr.petmeetingapi.Service;

import com.ardr.petmeetingapi.entity.Member;
import com.ardr.petmeetingapi.entity.Pet;
import com.ardr.petmeetingapi.model.Pet.PetCreatRequest;
import com.ardr.petmeetingapi.repository.PetRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PetService {

    private final PetRepository petRepository;
    public void setPet(Member member, PetCreatRequest request){
     Pet addDate = new Pet();
     addDate.setPetName(request.getPetName());
     addDate.setPetType(request.getPetType());
     addDate.setMember(member);
     petRepository.save(addDate);
    }
}
