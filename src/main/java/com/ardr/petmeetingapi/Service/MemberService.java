package com.ardr.petmeetingapi.Service;


import com.ardr.petmeetingapi.entity.Member;
import com.ardr.petmeetingapi.model.Member.MemberCreatRequest;
import com.ardr.petmeetingapi.model.Member.MemberItem;
import com.ardr.petmeetingapi.model.Member.MemberPhoneNumberChangeRequest;
import com.ardr.petmeetingapi.model.Member.MemberResponse;
import com.ardr.petmeetingapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {

    private final MemberRepository memberRepository;

    public Member getData(long id) {
        return memberRepository.findById(id).orElseThrow();
    }

    public void setMember(MemberCreatRequest request) {
        Member addData = new Member();
        addData.setOwnerName(request.getOwnerName());
        addData.setAge(request.getAge());
        addData.setIsMan(request.getIsMan());
        addData.setPhoneNumber(request.getPhoneNumber());
        memberRepository.save(addData);
    }

    public List<MemberItem> getMembers() {
        List<Member> originList = memberRepository.findAll();
        List<MemberItem> result = new LinkedList<>();
        for (Member member : originList) {
            MemberItem addItem = new MemberItem();

            addItem.setId(member.getId());
            addItem.setOwnerName(member.getOwnerName());
            result.add(addItem);
        }
        return result;
    }

    public MemberResponse getMember(long id) {

        Member originData = memberRepository.findById(id).orElseThrow();
        MemberResponse response = new MemberResponse();
        response.setId(originData.getId());
        response.setOwnerName(originData.getOwnerName());
        response.setAge(originData.getAge());
        response.setIsMan(originData.getIsMan() ? "남자":"여자");
        response.setPhoneNumber(originData.getPhoneNumber());

        return response;
    }



}
